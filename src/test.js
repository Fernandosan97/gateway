const axios = require('axios').default;

const main = async () => {
    try {
        let { data } = await axios.get("https://api.fernando-san-node.com")
        console.log(data)
    } catch (error) {
        console.log(error.toString())
    }
}

main()