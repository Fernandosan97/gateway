import "core-js/stable";
import "regenerator-runtime/runtime";
import moment from "moment";
import { io, server } from './server';

async function main() {
    await server.listen(443, async () => {
        console.log(`Listening on *: ${443}`, moment().format('DD/MM/YYYY HH:mm:ss'))
        io.on('connection', async (socket) => {
            console.log("connection: ", socket.id)
        })
    })
}

main()